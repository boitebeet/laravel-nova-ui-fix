import DangerButton from './components/DangerButton'
import LoadingButton from './components/LoadingButton'

Nova.booting(app => {
  app.component('DangerButton', DangerButton)
  app.component('LoadingButton', LoadingButton)
})
